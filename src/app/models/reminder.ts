export interface Reminder {
  id: number;
  date: Date;
  city: String;
  description: String;
  color: String;
}
