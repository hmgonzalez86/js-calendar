import { Reminder } from './reminder';

export interface DaySchedule {
  date: Date;
  reminders: Reminder[];
}
