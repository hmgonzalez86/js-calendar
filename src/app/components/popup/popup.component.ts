import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Reminder } from 'src/app/models/reminder';
import { DatePipe } from '@angular/common';
import { OpenweatherService } from 'src/app/services/openweather.service';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class PopupComponent implements OnInit {
  form: FormGroup;
  weather = {
    error: false,
    message: '',
  };

  colors = [
    { color: '#EF476F', value: 'color1' },
    { color: '#06D6A0', value: 'color2' },
    { color: '#FFD166', value: 'color3' },
    { color: '#118AB2', value: 'color4' },
  ];

  constructor(
    private _openWeather: OpenweatherService,
    private _formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private dialogRef: MatDialogRef<PopupComponent>,
    @Inject(MAT_DIALOG_DATA) reminder: Reminder
  ) {
    this.form = _formBuilder.group({
      id: [reminder.id],
      date: [
        this.datePipe.transform(reminder.date, 'yyyy-MM-dd'),
        Validators.required,
      ],
      time: [
        this.datePipe.transform(reminder.date, 'HH:mm'),
        Validators.required,
      ],
      city: [reminder.city, Validators.required],
      description: [reminder.description, Validators.required],
      color: [reminder.color],
    });
  }

  ngOnInit(): void {
    if (this.form.value.city !== '')
      this._openWeather
        .getWeather(this.form.value.city)
        .then((data: any) => {
          this.weather.error = false;
          this.weather.message = data.weather[0].description;
        })
        .catch((err: any) => {
          this.weather.error = true;
          this.weather.message = err.error.message;
          console.log(this.weather);
        });
  }

  onSubmit() {
    this.dialogRef.close({ submit: true, form: this.form.value });
  }

  onClose() {
    this.dialogRef.close({ submit: false });
  }
}
