import { Component, OnInit } from '@angular/core';
import { DaySchedule } from 'src/app/models/dayschedule';
import { Reminder } from 'src/app/models/reminder';
import { ReminderService } from 'src/app/services/reminder.service';
import {
  MatDialog,
  MatDialogRef,
  MatDialogConfig,
} from '@angular/material/dialog';
import { PopupComponent } from '../popup/popup.component';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css'],
})
export class CalendarComponent implements OnInit {
  actualDate: Date = new Date();

  months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];
  weekdays = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
  ];
  monthSchedule: DaySchedule[] = [];

  constructor(private _reminder: ReminderService, private dialog: MatDialog) {}

  ngOnInit(): void {
    this.generateCalendar();
  }

  previousMonth(): void {
    this.actualDate.setMonth(this.actualDate.getMonth() - 1);
    this.generateCalendar();
  }

  nextMonth(): void {
    this.actualDate.setMonth(this.actualDate.getMonth() + 1);
    this.generateCalendar();
  }

  generateCalendar() {
    this.monthSchedule = [];

    let firstDayOfMonth = new Date(this.actualDate);
    firstDayOfMonth.setDate(1);
    let lastDayOfMonth = new Date(this.actualDate);
    lastDayOfMonth.setMonth((firstDayOfMonth.getMonth() + 1) % 13);
    lastDayOfMonth.setDate(1);
    lastDayOfMonth.setDate(lastDayOfMonth.getDate() - 1);

    for (let i = 0; i < firstDayOfMonth.getDay(); i++) {
      let savedDate = new Date(firstDayOfMonth);
      savedDate.setDate(
        firstDayOfMonth.getDate() - (firstDayOfMonth.getDay() - i)
      );
      this.monthSchedule.push({ date: savedDate, reminders: [] });
    }

    for (
      let i = firstDayOfMonth;
      i <= lastDayOfMonth;
      i.setDate(i.getDate() + 1)
    ) {
      let savedDate = new Date(i);
      this.monthSchedule.push({
        date: savedDate,
        reminders: [],
      });
    }

    for (let i = 0; i < 6 - lastDayOfMonth.getDay(); i++) {
      let savedDate = new Date(lastDayOfMonth);
      savedDate.setDate(savedDate.getDate() + i + 1);
      this.monthSchedule.push({ date: savedDate, reminders: [] });
    }

    this.loadReminders();
  }

  loadReminders(): void {
    const reminders: Reminder[] = this._reminder
      .getReminders(this.actualDate.getMonth(), this.actualDate.getFullYear())
      .sort((a: Reminder, b: Reminder) => (a.date < b.date ? -1 : 0));
    reminders.forEach((reminder: Reminder) => {
      const daySchedule = this.monthSchedule.find(
        (day: DaySchedule) =>
          reminder.date.getDate() === day.date.getDate() &&
          reminder.date.getMonth() === day.date.getMonth() &&
          reminder.date.getFullYear() === day.date.getFullYear()
      );
      daySchedule?.reminders.push(reminder);
    });
  }

  isWeekend(date: Date): boolean {
    return date.getDay() === 0 || date.getDay() === 6;
  }

  isOutOfMonth(date: Date): boolean {
    return date.getMonth() !== this.actualDate.getMonth();
  }

  isToday(date: Date): boolean {
    const today = new Date();
    return (
      date.getDate() === today.getDate() && date.getMonth() === today.getMonth()
    );
  }

  openDialog(dialogConfig: MatDialogConfig): MatDialogRef<PopupComponent, any> {
    return this.dialog.open(PopupComponent, dialogConfig);
  }

  editDialog(reminder: Reminder) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.data = {
      id: reminder.id,
      date: reminder.date,
      city: reminder.city,
      description: reminder.description,
      color: reminder.color,
    };
    const dialogRef = this.openDialog(dialogConfig);
    dialogRef.afterClosed().subscribe((result) => {
      if (result.submit) {
        this._reminder.editReminder({
          id: result.form.id,
          date: new Date(result.form.date + 'T' + result.form.time),
          city: result.form.city,
          description: result.form.description,
          color: result.form.color,
        });
        this.generateCalendar();
      }
    });
  }

  newDialog(date: Date) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.data = {
      date: date,
      city: '',
      description: '',
      color: 'color1',
    };
    const dialogRef = this.openDialog(dialogConfig);
    dialogRef.afterClosed().subscribe((result) => {
      if (result.submit) {
        this._reminder.addReminder({
          id: -1,
          date: new Date(result.form.date + 'T' + result.form.time),
          city: result.form.city,
          description: result.form.description,
          color: result.form.color,
        });
        this.generateCalendar();
      }
    });
  }

  removeReminder(event: Event, reminder: Reminder): void {
    this._reminder.deleteReminder(reminder);
    this.generateCalendar();
    event.stopPropagation();
  }

  clearDay(date: Date): void {
    this._reminder.deleteReminderByDate(date);
    this.generateCalendar();
  }
}
