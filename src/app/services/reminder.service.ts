import { Injectable } from '@angular/core';
import { Reminder } from '../models/reminder';

@Injectable({
  providedIn: 'root',
})
export class ReminderService {
  private reminders: Reminder[] = [];

  constructor() {}

  getReminders(month: number, year: number): Reminder[] {
    return this.reminders.filter(
      (reminder) =>
        reminder.date.getMonth() === month &&
        reminder.date.getFullYear() === year
    );
  }

  addReminder(reminder: Reminder): void {
    if (this.reminders.length > 0) {
      const ids = this.reminders
        .map((reminder) => reminder.id)
        .sort((a: number, b: number) => a - b);
      const maxId = ids[ids.length - 1];
      reminder.id = maxId + 1;
    } else reminder.id = 0;

    this.reminders.push(reminder);
  }

  editReminder(reminder: Reminder): void {
    this.reminders[
      this.reminders.findIndex((r) => r.id === reminder.id)
    ] = reminder;
  }

  deleteReminder(reminder: Reminder): void {
    this.reminders = this.reminders.filter((r) => r.id !== reminder.id);
  }

  deleteReminderByDate(date: Date): void {
    this.reminders = this.reminders.filter(
      (r) =>
        r.date.getDate() !== date.getDate() &&
        r.date.getMonth() !== date.getMonth() &&
        r.date.getFullYear() !== date.getFullYear()
    );
  }
}
