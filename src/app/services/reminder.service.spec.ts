import { TestBed } from '@angular/core/testing';
import { Reminder } from '../models/reminder';
import { ReminderService } from './reminder.service';

describe('ReminderService', () => {
  let service: ReminderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReminderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should create a new reminder', () => {
    const date = new Date();  
    const totalDateReminders = service.getReminders(date.getMonth(), date.getFullYear()).length;
    const reminder: Reminder = {id: 0, date: date, city: 'Tandil,Argentina', description: 'A test reminder', color: 'color1' }
    service.addReminder(reminder);
    expect(service.getReminders(date.getMonth(), date.getFullYear()).length).toEqual(totalDateReminders+1);
  })
});