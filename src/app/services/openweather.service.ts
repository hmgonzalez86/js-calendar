import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class OpenweatherService {
  constructor(private httpClient: HttpClient) {}

  getWeather(city: string): Promise<any> {
    const APIkey = '42316ab47b406a53e5f6261f1e4e0217';
    return this.httpClient
      .get(
        `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${APIkey}`
      )
      .toPromise();
  }
}
